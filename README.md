# Diploma thesis: chrome extension

## Getting started

This is a GitLab repository of my diploma thesis, where I created a chrome exntesnion to manage proxies and use them in cooperation with other methods to mask users location and overall identity. 
<hr>

**Username:** kiralma1<br>
**Semester:** B221/B222<br>

## STRUCTURE:
- **thesis.pdf** - final version of the thesis
- **/LaTeX** - LaTeX files of the thesis
- **/src** - source code of the extension
    - **./assets/** - icons of the extension
    - **./bootstrap/** - local bootstrap library
    - **./popup/** - files managing the popup window of the extension
    - **./background.js** - file managing the background of the extension
    - **./constants.js** - all constants used in the extension
    - **./content_script.js** - code executed on webpages
    - **./db_utils.js** - utilities to manage the chrome storage
    - **./manifest.json** - manifest file of the extension
    - **./profile_validator.js** - utilities to validate profiles during import
    - **./rules.json** - exntensions default ruleset identifying its presence
- **/tester** - tool to visualize HTTP headers
- **./README.md** - this file
- **./run_remote_setup.sh** - script to run remote PROXY setup
- **./squid_proxy_setup_auth.sh** - script to setup PROXY with authentication
- **./squid_proxy_setup_open.sh** - script to setup open PROXY