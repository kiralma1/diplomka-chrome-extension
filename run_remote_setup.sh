#!/bin/bash

# AUTHOR: Marek Kiraly
# SCRIPT TO RUN SQUID PROXY SETUP ON REMOTE SERVER 
# DETERMINED BY IP ADDRESS IN $1 ARGUMENT


# Check if $2 is either 'open' or 'auth'
if [[ $2 == "open" || $2 == "auth" ]]; then
  # Check if the file /squid_proxy_setup{$2}.sh exists
  if [ -f "./squid_proxy_setup_${2}.sh" ]; then
    echo "File ./squid_proxy_setup_${2}.sh exists --> copying to remote server."

    # COPY SQUID PROXY SETUP SCRIPT TO REMOTE SERVER
    scp ./squid_proxy_setup_${2}.sh root@$1:/;

    # ADD EXECUTE PERMISSIONS TO SQUID PROXY SETUP SCRIPT ON REMOTE SERVER
    ssh root@$1 chmod +x /squid_proxy_setup_${2}.sh;

    # EXECUTE SQUID PROXY SETUP SCRIPT ON REMOTE SERVER
    ssh root@$1 bash /squid_proxy_setup_${2}.sh;

  else
    echo "File ./squid_proxy_setup_${2}.sh does not exist."
  fi
else
  echo "Error: param \$2 must be either 'open' or 'auth'."
fi



