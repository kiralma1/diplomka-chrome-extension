#!/bin/bash

# AUTHOR: Marek Kiraly
# SCRIPT TO SETUP SQUID OPEN PROXY ON UBUNTU 20.04 WITH STEP BY STEP COMMENTS

GREEN='\033[0;32m'
NC='\033[0m' # No Color

# UPDATE AND INSTALL SQUID
sudo apt-get update;
sudo apt-get install --yes squid;
echo -e "${GREEN}Squid installed ---------------------------------------------${NC}";

# PREPARING SQUID CONFIG AND PROTECTING ORIGINAL
echo -e "${GREEN}Preparing squid config --------------------------------------${NC}";
sudo cp /etc/squid/squid.conf /etc/squid/squid.conf.original;
sudo chmod a-w /etc/squid/squid.conf.original;
ls -l /etc/squid/squid.conf*;
echo -e "${GREEN}Squid config prepared ---------------------------------------${NC}";

# SQUID CONFIG CHANGES
echo -e "${GREEN}Applying squid config changes -------------------------------${NC}";
sed -i 's/http_access deny all/http_access allow all/' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "http_access allow all";

sed -i 's/# dns_v4_first off/dns_v4_first on/' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "dns_v4_first on";

echo -e "${GREEN}Squid config changes applied --------------------------------${NC}";

# REMOVE REDUNDANT LINES
sed -Ei '/(^$|^#)/d' /etc/squid/squid.conf
echo -e "${GREEN}Redundant lines removed --------------------------------${NC}";

# RESTART SQUID TO APPLY CHANGES AND CHECK STATUS
echo -e "${GREEN}Restarting squid and checking status ------------------------${NC}";
systemctl restart squid;
systemctl status squid | grep "Active:";
echo -e "${GREEN}Squid restarted and status checked --------------------------${NC}";