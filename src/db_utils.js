import cs from './constants.js'

// storage functions ==========================================================
// function to get a value from chrome.storage.sync
function syncGet(key) {
    return new Promise(resolve => {
        chrome.storage.sync.get([key], function (items) {
            if (chrome.runtime.error || !(key in items)) resolve(undefined);
            else resolve(items[key]);
        });
    });
}

// function to get a value from chrome.storage.local
function localGet(key) {
    return new Promise(resolve => {
        chrome.storage.local.get([key], function (items) {
            if (chrome.runtime.error || !(key in items)) resolve(undefined);
            else resolve(items[key]);
        });
    });
}

// function to set a value in chrome.storage.sync
function syncSet(key, value) {
    return new Promise(resolve => {
        chrome.storage.sync.set({ [key]: value }, function () {
            if (chrome.runtime.error) resolve(false);
            else resolve(true);
        });
    });
}

// function to set a value in chrome.storage.local
function localSet(key, value) {
    return new Promise(resolve => {
        chrome.storage.local.set({ [key]: value }, function () {
            if (chrome.runtime.error) resolve(false);
            else resolve(true);
        });
    });
}

// function to clear chrome.storage.sync
function syncClear() {
    return new Promise(resolve => {
        chrome.storage.sync.clear(function () {
            if (chrome.runtime.error) resolve(false);
            else resolve(true);
        });
    });
}

// function to update UI states in chrome.storage.local
async function localUpdateStates(key, value) {
    let local = await localGet(cs.STATES_KEY);
    local[key] = value;
    return localSet(cs.STATES_KEY, local);
}

// function to toggle UI states in chrome.storage.local
async function localToggleState(key) {
    let local = await localGet(cs.STATES_KEY);
    if (local){
        if (local[key]) {
            local[key] = !local[key]
        }
        else{   
            local[key] = true
        }
    }
    else {
        local = {}
        local[key] = true
    }
    localSet(cs.STATES_KEY, local);
    return local[key]
}

// function to get all values from chrome.storage.sync with a given prefix
function syncGetAll(prefix) {
    return new Promise((resolve, reject) => {
        chrome.storage.sync.get(null, function (items) {
            let result = {};
            for (let header in items) {
                if (header.startsWith(prefix)) {
                    result[header] = items[header];
                }
            }
            resolve(result);
        });
    });
}

// function to remove all values starting with a given prefix from chrome.storage.sync
async function syncRemoveAll(prefix) {
    return new Promise((resolve, reject) => {
        chrome.storage.sync.get(null, async function (items) {
            let result = {};
            for (let header in items) {
                if (header.startsWith(prefix)) {
                    await syncRemove(header);
                }
            }

            chrome.storage.sync.remove(Object.keys(result), function () {
                if (chrome.runtime.error) resolve(false);
                else resolve(true);
            });
        });
    });
}


// function to get all values from chrome.storage.local with a given prefix
function localGetAll(prefix) {
    return new Promise((resolve, reject) => {
        chrome.storage.local.get(null, function (items) {
            let result = {};
            for (let header in items) {
                if (header.startsWith(prefix)) {
                    result[header] = items[header];
                }
            }
            resolve(result);
        });
    });
}

// function to remove a value from chrome.storage.sync
function syncRemove(key) {
    return new Promise(resolve => {
        chrome.storage.sync.remove(key, function () {
            if (chrome.runtime.error) resolve(false);
            else resolve(true);
        });
    });
}

// make functions available to other files
export default {
    localGet,
    localGetAll,
    localSet,
    localUpdateStates,
    localToggleState,
    syncGet,
    syncSet,
    syncRemove,
    syncClear,
    syncGetAll,
    syncRemoveAll
};
    