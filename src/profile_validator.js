// function to check if a string is a valid header name
function is_valid_header_name(name) {
    const regex = /^[!#$%&'*+\-.^_`|~0-9A-Za-z]+$/;
    return regex.test(name);
}

// function to check if a string is a valid IP address
function is_valid_IP(ip) {
    const regex = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])$/;
    return regex.test(ip);
}

// function to check if a string is a valid port number
function is_valid_port(port) {
    const portNum = parseInt(port, 10);
    return portNum > 0 && portNum <= 65535;
}

// function to check if a profile structure is valid for import
function validate_profile_structure(structure) {
    try{
        let chs = [];
        let cps = []
        let checkedProxy = null;
        let cps_ip_port = []

        if (typeof structure !== "object") return false;
        if (structure === null) return false;
        if (typeof structure["ac-lang"] !== "string") return false;
        if (typeof structure.user_agent !== "string") return false;

        for (const key in structure) {
            if (key.startsWith('ch-h')) {
                const x = parseInt(key.substring(4), 10);
                if (isNaN(x) || x < 1) return false;
                
                if (chs.includes(x)) return false;
                chs.push(x);

                const ch = structure[key];
                if (ch.id !== `h${x}` || !is_valid_header_name(ch.key)) return false;

            } else if (key.startsWith('cp-p')) {
                const x = parseInt(key.substring(4), 10);
                if (isNaN(x)) return false;

                if (cps.includes(x)) return false;
                cps.push(x);
                
                const cp = structure[key];
                if (cp.id !== `p${x}` || !is_valid_IP(cp.key) || !is_valid_port(cp.value)) return false;
                if (cp.checked === "true") {
                    if (checkedProxy) return false;
                    checkedProxy = cp.id;
                }
                cps_ip_port.push(cp.key + ":" + cp.value)

            }
        }

        if (structure.checked_proxy !== null && structure.checked_proxy !== checkedProxy) return false;

        if (structure.using_proxy === "proxy") {
            if ((!is_valid_IP(structure.proxy.host) && structure.proxy.host !== "null") || 
                (!is_valid_port(structure.proxy.port) && structure.proxy.host !== "null") || 
                (structure.proxy.host !== "null" || structure.proxy.port !== "null")) return false;

        } else if (structure.using_proxy === "custom_proxy") {
            if (structure.custom_proxy.checked != false && structure.custom_proxy.host != "null" && structure.custom_proxy.port !== "null"  && 
                !cps_ip_port.includes(structure.custom_proxy.host + ":" + structure.custom_proxy.port) ) return false;

        } else if (structure.using_proxy !== "null") {
            return false;
        }
        return true;
    }
    catch(err){
        return false;
    }
}

// make functions available to other files
export default {
    validate_profile_structure
}