import cs from "./constants.js"
import db from "./db_utils.js"

console.log(`[${new Date().toLocaleString()}] background.js is running`)

// set the DOM language for the displayed tab content
function set_locale(tabid, val){
    try{    
        // Send a message to the content script with the lang value
        chrome.tabs.sendMessage(tabid, {
            type: 'setLang',
            lang: val
        });
    }
    catch{
        console.log("DOM unavailable")
    }
}

async function get_current_tab() {
    let tab = await chrome.tabs.query({ active: true, lastFocusedWindow: true });
    return tab[0];
}

async function get_all_tabs(){
    var tabs = await chrome.tabs.query({});
    return tabs
}

// funcction to retrieve the data from given url, post process it and store it in the storage under the given key
async function get_data(url, storage_key) {
    console.log(`[${new Date().toLocaleString()}] Retrieving data from ${url} and storing it under ${storage_key} key`);
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };

    fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) {
                throw new Error('Error retrieving langs.json data');
            }
            return response.json();
        })
        .then(async data => {
            db.localSet(storage_key, data);

        })
        .catch(error => console.log(`[${new Date().toLocaleString()}] Error: ${error}`));
}

// Used the class approach to better manage the code and mark the up-to-date-ness of the 
// rules, as the rules are returned asynchronously without the need of a global variable.
// Getting the rules only if they are not up to date greatly improves the performance of the extension.
// class handling all the backend logic for the extension
class BackendManager {
    constructor() {
        // flag to check if the rules are up to date with the storage
        this.up_to_date_storage = false
        // currently downloaded rules
        this.rules = []
        this.finished_rules_processing = null
        this.ids = []
        for (let i = 3; i < 2000; i++) {
            this.ids.push(i)
        }
        this.auth_required_response = null
    }

    // function to get new id
    get_new_id() {
        let ret = this.ids.shift()
        if (!this.ids.length) {
            for (let i = 2000; i <= 4000; i++) {
                this.ids.push(i)
            }
        }
        return ret
    }

    // function to return id
    return_id(id) {
        this.ids.push(id)
    }

    async set_on_auth_required() {
        let current_proxy_type = await db.syncGet(cs.USING_PROXY_STORAGE_KEY)
        let current_proxy = await db.syncGet(current_proxy_type === undefined ? cs.PROXY : current_proxy_type)
        this.auth_required_response = null

        if (current_proxy === undefined) {
            return
        }

        let gsp_exclusive_proxies = await db.localGet(cs.GSP_EXCLUSIVE_PROXIES_DB)
        if (gsp_exclusive_proxies && Object.keys(gsp_exclusive_proxies).length > 0){

            // iterate through all country, object pairs
            for (const [country, obj] of Object.entries(gsp_exclusive_proxies)) {
                // iterate through all proxies in the country
                for (const proxy of obj.proxies) {
                    if (current_proxy.host === proxy.host && parseInt(current_proxy.port) === proxy.port) {
                        this.auth_required_response = { authCredentials: { username: "admin", password: "Praha2023Zurych" } }
                    }
                }
            }
        }

    }
    // function to extract the header key from a rule object
    rule_header(rule) {
        return rule.action.requestHeaders[0].header;
    }

    // function to extract the rule type from a rule object
    rule_type(rule) {
        return rule.action.type;
    }

    // function to get current dynamic rules from the storage
    async get_dynamic_rules() {
        // if the rules are not up to date, get them from the storage, update the flag and store them in the class
        if (!this.up_to_date_storage) {
            this.rules = new Promise((resolve, reject) => {
                chrome.declarativeNetRequest.getDynamicRules((data) => {
                    resolve(data);
                });
            });
            this.up_to_date_storage = true
        }

        return this.rules
    }

    // function to rebuild existing rule object with a new value
    rebuild_rule(action, rule, rule_value = null) {
        let obj = {}

        // if the action is to set a new value, rebuild the rule object with the new value
        if (action === cs.SET_ACTION) {
            obj = {
                addRules: [
                    {
                        id: rule.id,
                        priority: rule.priority,
                        action: {
                            type: rule.action.type,
                            requestHeaders: [
                                {
                                    header: this.rule_header(rule),
                                    operation: action,
                                    value: rule_value
                                },
                            ],
                        },
                        condition: {
                            urlFilter: rule.condition.urlFilter,
                            resourceTypes: rule.condition.resourceTypes,
                        },
                    },
                ],
                removeRuleIds: [rule.id]
            }
        }
        // if the action is to remove the rule, store only the id of the rule to be removed
        else if (action === cs.REMOVE_ACTION) {
            obj = {
                addRules: [],
                removeRuleIds: [rule.id]
            }
        }
        return obj
    }

    // function to build a new rule object
    build_new_rule(action, rule_id, header = null, rule_value = null, action_type = cs.MODIFY_HEADERS_ACTION, rule_priority = 1) {
        let obj = {}

        // if the action is to set a new value, build a new rule object
        if (action === cs.SET_ACTION) {
            obj = {
                addRules: [
                    {
                        id: rule_id,
                        priority: rule_priority,
                        action: {
                            type: action_type,
                            requestHeaders: [
                                {
                                    header: header,
                                    operation: action,
                                    value: rule_value
                                },
                            ],
                        },
                        condition: {
                            urlFilter: "|http*",
                            resourceTypes: ['main_frame', 'sub_frame', 'script'],
                        },
                    },
                ],
                removeRuleIds: [rule_id]
            }
        }
        // if the action is to remove the rule, store only the id of the rule to be removed
        else if (action === cs.REMOVE_ACTION) {
            obj = {
                addRules: [],
                removeRuleIds: [rule_id]
            }
        }
        return obj
    }

    // function to get the rule object from the dynamic rules for a given header key
    async get_rule_for_header(header) {
        // get the current dynamic rules
        const rules = await this.get_dynamic_rules()

        // iterate through the rules and return the rule object for the given header
        for (let i = 0; i < rules.length; i++) {
            if (this.rule_header(rules[i]) === header) {
                return rules[i]
            }
        }

        return null

    }

    // function to get the smallest available id for a new rule
    async new_rule_id() {
        // default rule from rules.json has id 1, therefore smallets the id can get is 2
        let ret_id = 2
        // get the current dynamic rules
        let rules = await this.get_dynamic_rules()

        // sort rules by rule[i].id so the sallest available id can be found
        rules.sort((a, b) => (a.id > b.id) ? 1 : -1)

        // iterate through the rules and find the smallest available id
        for (var i = 0; i < rules.length; i++) {
            if (ret_id === rules[i].id - 1) {
                break
            }
            else {
                ret_id++;
            }
        };

        return ret_id
    }

    // function to update the rule for a given header key based on the action
    async update_header_rule(header, new_value, action) {
        // getting the rule for the given header if it exists
        let rule = await this.get_rule_for_header(header)
        let id = -1

        // if the rule does not exist, create a new rule
        if (rule === null) {
            // id = await this.new_rule_id()
            id = this.get_new_id()
            chrome.declarativeNetRequest.updateDynamicRules(this.build_new_rule(action, id, header, new_value));
        }
        // if the rule exists, update the rule
        else {
            id = rule.id
            if (action === cs.REMOVE_ACTION) {
                this.return_id(id)
            }
            chrome.declarativeNetRequest.updateDynamicRules(this.rebuild_rule(action, rule, new_value));
        }

        // update the flag to indicate that the rules are not up to date
        this.up_to_date_storage = false
    }

    // function to process the changes to any rule for a predefined header
    async process_predefined_value_rule(header, new_value) {
        let rule_action = null;
        // decide the action based on the new value
        if (new_value !== "null") {
            rule_action = cs.SET_ACTION;
        }
        else {
            rule_action = cs.REMOVE_ACTION;
        }

        // update the rule
        await this.update_header_rule(header, new_value, rule_action)
    }

    // function to process the changes to any rule for a custom header
    async process_Custom_Header_rule(changes) {
        // updating an existing rule
        if ("newValue" in changes && "oldValue" in changes) {
            // if the key of the rule has changed, remove the rule with the old key
            if (changes["newValue"].key !== changes["oldValue"].key) {
                await this.update_header_rule(
                    changes["oldValue"].key,
                    null,
                    cs.REMOVE_ACTION
                )
            }
            // if the rule is checked, use the new value to build the rule
            if (changes["newValue"].checked) {
                await this.update_header_rule(
                    changes["newValue"].key,
                    changes["newValue"].value.replace(" ", "_"),
                    cs.SET_ACTION
                )
            }
            // else remove the rule as the user has disabled it's usage
            else {
                await this.update_header_rule(
                    changes["newValue"].key,
                    null,
                    cs.REMOVE_ACTION
                )
            }
        }
        // deleting or adding a new rule
        else {
            let header = null;
            let rule_action = null;
            let new_value = null;

            // this means a new rule is being added and it is checked to be used
            if ("newValue" in changes && changes["newValue"].checked) {
                // set the header key and the new value
                header = changes["newValue"].key;
                rule_action = cs.SET_ACTION;
                new_value = changes["newValue"].value.replace(" ", "_");
            }
            // this means a rule is being deleted
            else if ("oldValue" in changes) {
                // set the header key and the action to remove the rule
                header = changes["oldValue"].key;
                rule_action = cs.REMOVE_ACTION;
            }

            // add/delete the rule if the action has been decided
            if (rule_action) {
                await this.update_header_rule(header, new_value, rule_action)
            }
        }
    };

    // function to process the proxy settings
    async process_Proxy_settings() {
        // get the info if user is using a pre-defined proxy or a custom proxy
        let active_proxy = await db.syncGet(cs.USING_PROXY_STORAGE_KEY)
        // get info about the currently active proxy
        let proxy_info = await db.syncGet(active_proxy === undefined ? cs.PROXY_STORAGE_KEY : active_proxy)
        let is_custom_proxy = active_proxy === cs.CUSTOM_PROXY_STORAGE_KEY
        
        // if the user is not using a proxy, clear the proxy settings
        if (!proxy_info || proxy_info?.host === 'null' || proxy_info?.key === 'null') {
            chrome.proxy.settings.clear(
                { scope: 'regular' },
                function () { }
            );
        }
        // else set the proxy settings for both http and https
        else if (proxy_info && (!is_custom_proxy || (is_custom_proxy && proxy_info?.checked))) {
            var config = {
                mode: "fixed_servers",
                rules: {
                    proxyForHttp: {
                        host: proxy_info.host || proxy_info.key,
                        port:  parseInt(proxy_info.port) || parseInt(proxy_info.value),
                    },
                    proxyForHttps: {
                        host: proxy_info.host || proxy_info.key,
                        port: parseInt(proxy_info.port) || parseInt(proxy_info.value),
                    }
                }
            }

            // apply the proxy settings
            chrome.proxy.settings.set(
                { value: config, scope: 'regular' },
                function () { }
            );
        }
        await this.set_on_auth_required()
    };
}



// ============================================================================================================================================================================
// MAIN =======================================================================================================================================================================
// ============================================================================================================================================================================

// create a new instance of the BackendManager class
let be_manager = new BackendManager()
be_manager.set_on_auth_required()

// set the default values for the storage
db.syncSet(cs.DATA_DOWNLOAD_PERIOD_STORAGE_KEY, cs.DATA_DEFAULT_DOWNLOAD_PERIOD)
db.syncSet(cs.PROXY_PING_PERIOD_STORAGE_KEY, cs.PROXY_DEFAULT_PING_PERIOD)

// get the initial data from the sources
get_data(cs.LANGS_URL, cs.LANGS_DB);
get_data(cs.USER_AGENTS_URL, cs.USER_AGENTS_DB);
get_data(cs.PROXIES_URL, cs.PROXIES_DB);
get_data(cs.GSP_EXCLUSIVE_PROXIES_URL, cs.GSP_EXCLUSIVE_PROXIES_DB);

// listen for changes to the storage
chrome.storage.onChanged.addListener(async function (changes, namespace) {
    // console.log(`[${new Date().toLocaleString()}] Storage ${namespace} changed`)

    // check if changes contains the Accept-Language header rule
    if (cs.ACCEPT_LANGUAGE_STORAGE_KEY in changes && namespace === `sync`) {
        let tmp_val = changes[cs.ACCEPT_LANGUAGE_STORAGE_KEY].newValue
        if (tmp_val.split("-").length > 1) {
            tmp_val = `${tmp_val};q=1.0,${tmp_val.split("-")[0]};q=0.9`
        }
        await be_manager.process_predefined_value_rule(cs.ACCEPT_LANGUAGE_HEADER, tmp_val)
        
        // let tab = await get_current_tab()
        let tabs = await get_all_tabs()
        tabs.forEach(async tab => {
            set_locale(tab.id, tmp_val)
        });
    }

    // check if changes contains string starting withc "ch-" (*c*ustom *h*eader rules)
    let ch_header = Object.keys(changes).filter(key => key.startsWith("ch-"));
    if (ch_header.length) {
        await be_manager.process_Custom_Header_rule(changes[ch_header])
    }

    // check if the user chose to use a pre-defined proxy or a custom proxy or if the used proxy type changed
    if ((cs.USING_PROXY_STORAGE_KEY in changes ||
        cs.PROXY_STORAGE_KEY in changes ||
        cs.CUSTOM_PROXY_STORAGE_KEY in changes ) && 
        namespace === `sync`) {

        await be_manager.process_Proxy_settings()
    }

    // check if changes contains the user-agent header rule
    if (cs.USER_AGENT_STORAGE_KEY in changes && namespace === `sync`) {
        await be_manager.process_predefined_value_rule(cs.USER_AGENT_HEADER, changes[cs.USER_AGENT_STORAGE_KEY].newValue)
    }
});


// Create an alarm to download the data every cs.DATA_DEFAULT_DOWNLOAD minutes
chrome.alarms.create(cs.DATA_DOWNLOAD_ALARM, {
    periodInMinutes: cs.DATA_DEFAULT_DOWNLOAD_PERIOD
});

// Create an alarm to ping the proxies every cs.PROXY_DEFAULT_PING minutes
chrome.alarms.create(cs.PROXY_PING_ALARM, {
    periodInMinutes: cs.PROXY_DEFAULT_PING_PERIOD
});

// Listen for alarms
chrome.alarms.onAlarm.addListener(async function (alarm) {
    if (alarm.name === cs.DATA_DOWNLOAD_ALARM) {
        await get_data(cs.LANGS_URL, cs.LANGS_DB);
        await get_data(cs.USER_AGENTS_URL, cs.USER_AGENTS_DB);
        await get_data(cs.GSP_EXCLUSIVE_PROXIES_URL, cs.GSP_EXCLUSIVE_PROXIES_DB);

        chrome.storage.sync.get(cs.DATA_DOWNLOAD_PERIOD_STORAGE_KEY, function (data) {
            const delayInMinutes = data[cs.DATA_DOWNLOAD_PERIOD_STORAGE_KEY] || cs.DATA_DEFAULT_DOWNLOAD_PERIOD;
            chrome.alarms.create(cs.DATA_DOWNLOAD_ALARM, { delayInMinutes });
        });

    }
    else if (alarm.name === cs.PROXY_PING_ALARM) {
        await get_data(cs.PROXIES_URL, cs.PROXIES_DB);

        chrome.storage.sync.get(cs.PROXY_PING_PERIOD_STORAGE_KEY, function (data) {
            const delayInMinutes = data[cs.PROXY_PING_PERIOD_STORAGE_KEY] || cs.PROXY_DEFAULT_PING_PERIOD;
            chrome.alarms.create(cs.PROXY_PING_ALARM, { delayInMinutes });
        });
    }
});

function creds() {
    if (be_manager.auth_required_response){
        console.log(`[${new Date().toLocaleString()}] Auth credential provided]`)
        
        chrome.notifications.create({
            type: 'basic',
            iconUrl: '../assets/logo128.png',
            title: 'Credentials used',
            message: 'You have been autenticated to use GSP exclusive proxy',
        }, function(notificationId) {
            console.log('Notification created with ID:', notificationId);
        });
            
        return be_manager.auth_required_response;
    }
}
  
chrome.webRequest.onAuthRequired.addListener(
    creds,
    {urls: ["<all_urls>"]},
    ["blocking"]
);

chrome.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
    if (changeInfo.status === 'complete') {
        let lang = await db.syncGet(cs.ACCEPT_LANGUAGE_STORAGE_KEY)
        if (lang.split("-").length > 1) {
            lang = `${lang};q=1.0,${lang.split("-")[0]};q=0.9`
        }
        set_locale(tabId, lang)
    }
  });

// ============================================================================================================================================================================
// print all item is sync storage
// ===============================
// chrome.storage.sync.get(null, function(items) {
//     var allKeys = Object.keys(items);
//     for (const [index, key] of Object.entries(allKeys)) {
//         chrome.storage.sync.get([key], function (items) {
//             console.log(`${key}: `, items[key])
//         });
//     }
// });

// print all item is local storage
// ===============================
// chrome.storage.local.get(null, function(items) {
//     var allKeys = Object.keys(items);
//     for (const [index, key] of Object.entries(allKeys)) {
//         chrome.storage.local.get([key], function (items) {
//             console.log(`${key}: `, items[key])
//         });
//     }
// });
