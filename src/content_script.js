(() => {
	console.log("Proxy extension v" + chrome.runtime.getManifest().version + " runnning");


	chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
		if (message.type === 'setLang') {
			const lang = message.lang !== "null" ? message.lang : navigator.language;
			document.documentElement.lang = lang;
			
		}
	});
})();   
