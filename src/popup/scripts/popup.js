// import constants
import cs from '../../constants.js'
// import db utils
import db from "../../db_utils.js"

//import profile_validator
import pv from "../../profile_validator.js"

console.log(`[${new Date().toLocaleString()}] popup.js is running`)

// UI functions ==============================================================================================================
// function to populate the profile list with the available saved profiles
async function populate_profiles_list(){
    $(".profile_from_list").remove();
    // get the current profile name from the storage
    let curr_name = await db.syncGet(cs.CURRENT_PROFILE_STORAGE_KEY)
    if (curr_name === undefined || curr_name === cs.DEFAULT_PROFILE){
        curr_name = cs.DEFAULT_PROFILE
        await db.syncSet(cs.CURRENT_PROFILE_STORAGE_KEY, curr_name)
        await create_sync_storage_snapshot(cs.PROFILES_STORAGE_KEY_PREFIX + curr_name)
    }
    display_profile_name(curr_name)

    let is_default_profile = curr_name === cs.DEFAULT_PROFILE

    $('.btn-proxy_collapser').prop('disabled', !is_default_profile)
    $('.btn-proxy_collapser').toggleClass("disabled_cursor", !is_default_profile)
        
    $('.btn-headers_collapser').prop('disabled', !is_default_profile)
    $('.btn-headers_collapser').toggleClass("disabled_cursor", !is_default_profile)
    
    // hide or show the add new profile button based on the is_default_profile value
    $('#add_new_profile').prop('hidden', !is_default_profile)

    // get all profiles from the storage
    let profiles = await db.syncGetAll(cs.PROFILES_STORAGE_KEY_PREFIX);
    // sort the profiles by their names
    let profiles_sorted = Object.fromEntries(Object.entries(profiles).sort())
    // iterate over the sorted languages and add them to the list
    $.each(profiles_sorted, function (name, profile) {
        // if the current language is the same as the one in the list, mark it as selected
        if (curr_name === name) {
            $('#profiles_list').append($("<option></option>")
                .attr("class", "profile_from_list")
                .attr("value", name)
                .attr("selected", true)
                .text(name.replace(cs.PROFILES_STORAGE_KEY_PREFIX, "")));
        }
        // otherwise, just add it to the list
        else {
            $('#profiles_list').append($("<option></option>")
            .attr("class", "profile_from_list")
            .attr("value", name)
            .text(name.replace(cs.PROFILES_STORAGE_KEY_PREFIX, "")));
        }

    });

    //  display current profile in the UI
    let profile = await db.syncGet(curr_name)
    await display_profile(profile, curr_name)
}

// function to populate the language list with the available predefined languages
async function populate_langs_list() {
    $(".lang_option").remove();
    // get the current language from the storage
    let curr_lang = await db.syncGet(cs.ACCEPT_LANGUAGE_STORAGE_KEY);

    // get current langs list from the storage
    let langs = await db.localGet(cs.LANGS_DB)
    // sort the languages by country name
    let langs_sorted = Object.fromEntries(Object.entries(langs).sort())
    // iterate over the sorted languages and add them to the list
    $.each(langs_sorted, function (country, code) {
        // if the current language is the same as the one in the list, mark it as selected
        if (code == curr_lang) {
            $('#langs_list').append($("<option class='lang_option'></option>")
                .attr("value", code)
                .attr("selected", true)
                .text(country));
        }
        // otherwise, just add it to the list
        else {
            $('#langs_list').append($("<option class='lang_option'></option>").attr("value", code).text(country));
        }
    });
}

// function to populate the proxy list with the available predefined proxies
async function populate_proxy_list() {
    $(".proxy_option").remove();
    $("#proxy_list optgroup").remove()
    let proxies_groups_sorted = null
    // get the current proxy from the storage
    let curr_proxy = await db.syncGet(cs.PROXY)
    
    // get the special GSP proxies from the storage --------------------------------
    let gsp_exclusive_proxies = await db.localGet(cs.GSP_EXCLUSIVE_PROXIES_DB)
    if (gsp_exclusive_proxies && Object.keys(gsp_exclusive_proxies).length > 0){
        // sort the proxies by country name
        proxies_groups_sorted = Object.fromEntries(Object.entries(gsp_exclusive_proxies).sort())
        
        // iterate over the sorted proxies and add them to the list grouped by country in a separate Open Proxies optgroup
        $('#proxy_list').append($("<optgroup disabled></optgroup>").attr("class", "gsp_proxies").attr("label", `GSP Exclusive Proxies_____`))
        
        $.each(proxies_groups_sorted, function (country, obj) {
            $('#proxy_list').append($("<optgroup></optgroup>")
                .attr("class", "public_" + country + "_opt_group")
                .attr("label", obj.flag + " - " + country))

            // sort the proxies by ip
            let proxies_sorted = Object.fromEntries(Object.entries(obj.proxies).sort(function (a, b) { return a.ip < b.ip }))
            $.each(proxies_sorted, function (key, proxy) {
                // if the current proxy is the same as the one in the list, mark it as selected
                if (proxy.host == curr_proxy?.host) {
                    $(`.public_${country}_opt_group`).append($("<option class='proxy_option'></option>")
                        .attr("value", proxy.host)
                        .attr("selected", true)
                        .attr("port", proxy.port)
                        .text(`${proxy.name} - ${proxy.host}`));
                }
                // otherwise, just add it to the list
                else {
                    $(`.public_${country}_opt_group`).append($("<option class='proxy_option'></option>")
                        .attr("value", proxy.host)
                        .attr("port", proxy.port)
                        .text(`${proxy.name} - ${proxy.host}`));
                }
            })
        });
    }
    
    // get current proxies list from the storage --------------------------------
    let proxies = await db.localGet(cs.PROXIES_DB)
    if (proxies && Object.keys(proxies).length > 0){
        // sort the proxies by country name
        proxies_groups_sorted = Object.fromEntries(Object.entries(proxies).sort())
        // iterate over the sorted proxies and add them to the list grouped by country in a separate Open Proxies optgroup
        $('#proxy_list').append($("<optgroup disabled></optgroup>").attr("class", "open_proxies").attr("label", "Open Proxies______________"))
        $.each(proxies_groups_sorted, function (country, obj) {
            if (obj.proxies.length > 0){
                country = country.replace(" ", "_")
                $('#proxy_list').append($("<optgroup></optgroup>")
                    .attr("class", "gsp_" + country + "_opt_group")
                    .attr("label", obj.flag + " - " + country))

                let proxies_sorted = Object.fromEntries(Object.entries(obj.proxies).sort(function (a, b) { return a.ip < b.ip }))
                $.each(proxies_sorted, function (key, proxy) {
                    // if the current proxy is the same as the one in the list, mark it as selected
                    if (proxy.host == curr_proxy?.host) {
                        $(`.gsp_${country}_opt_group`).append($("<option class='proxy_option'></option>")
                            .attr("value", proxy.host)
                            .attr("selected", true)
                            .attr("port", proxy.port)
                            .text(`${proxy.name} - ${proxy.host}`));
                    }
                    // otherwise, just add it to the list
                    else {
                        $(`.gsp_${country}_opt_group`).append($("<option class='proxy_option'></option>")
                            .attr("value", proxy.host)
                            .attr("port", proxy.port)
                            .text(`${proxy.name} - ${proxy.host}`));
                    }
                })
            }
        });
    }    
}

// function to populate the user_agent list with the available predefined user_agents
async function populate_user_agent_list() {
    $(".user_agent_option").remove();
    $("#user_agents_list optgroup").remove()
    // get the current user_agent from the storage
    let curr_user_agent = await db.syncGet(cs.USER_AGENT_STORAGE_KEY)
    // get current user_agents list from the storage
    let user_agents = await db.localGet(cs.USER_AGENTS_DB)
    // sort the user_agents by name
    let user_agents_sorted = Object.fromEntries(Object.entries(user_agents).sort())
    // iterate over the sorted user_agents and add them to the list
    $.each(user_agents_sorted, function (name, user_agent) {
        $('#user_agents_list').append($("<optgroup></optgroup>")
            .attr("class", name.replace(" ", "_") + "_opt_group")
            .attr("label", name)) 
        
        let user_agents_sorted = Object.fromEntries(Object.entries(user_agent).sort(function (a, b) { return a < b }))
        $.each(user_agents_sorted, function (key, user_agent) {
            // if the current user_agent is the same as the one in the list, mark it as selected
            if (user_agent == curr_user_agent) {
                $(`.${name.replace(" ", "_")}_opt_group`).append($("<option class='user_agent_option'></option>")
                    .attr("value", user_agent)
                    .attr("selected", true)
                    .text(key));
            }
            // otherwise, just add it to the list
            else {
                $(`.${name.replace(" ", "_")}_opt_group`).append($("<option class='user_agent_option'></option>")
                    .attr("value", user_agent)
                    .text(key));
            }
        })
    })
};

// function to populate the custom headers and custom proxies lists
async function populate_custom_records(type){
    // get all the custom items from the storage based on the type
    let custom_entries = await db.syncGetAll(`c${type.substr(0,1)}-`);

    // iterate over the custom items and add them to the list
    if (type == cs.HEADER){
        $(".custom_header_row").remove();
    }
    else if (type == cs.PROXY){
        $(".custom_proxy_row").remove();
    }
    for (let [key, value] of Object.entries(custom_entries)) {
        // if the item is a header, add it to the headers list
        if (type == cs.HEADER){
            add_custom_header_row(value.checked, false, value.id)
        }
        // if the item is a proxy, add it to the proxies list
        else if (type == cs.PROXY){
            add_custom_proxy_row(value.checked, false, value.id)
        }

        // set initial visible and meta values
        $(`#${value.id} .first`).val(value.key).attr("original_value", value.key)
        $(`#${value.id} .second`).val(value.value).attr("original_value", value.value)        
        $(`#${value.id} .custom_on_of`).attr("original_value", value.checked);
    }

    // add the listeners to the custom items
    add_custom_row_listeners()
};

// function to add a new custom header row to the headers list
function add_custom_header_row(checked, disabled, id) {
    // add the new header row http struct to the headers list
    let row_str = `<div class="input-group input-group-sm mb-3 custom_header_row" id="${id}">
                    <div class="input-group-prepend">
                        <div class="input-group-text custom_header_on_of custom_on_of">
                            <span data-toggle="tooltip" data-placement="top" title="Enable this option to add a custom header to your browsing traffic. A header is a small piece of information sent with each request to a website, and can be used to personalize your browsing experience.">
                                <input type="checkbox" ${checked ? "checked" : ""}>
                            </span>
                        </div>
                    </div>

                    <input placeholder="header" type="text" class="form-control header first custom_input">
                    <input placeholder="header value" type="text" class="form-control header-value second custom_input ">

                    <div class="input-group-append">
                        <button class="btn btn-primary custom_save ${disabled ? "disabled_cursor" : ""}" type="button" ${disabled ? "disabled" : ""}>Save</button>
                        <button class="btn btn-danger custom_delete" type="button">Delete</button>
                    </div>
                </div>`


    if ($('.custom_header_row').length == 0){
        $(".custom_headers").prepend(row_str);
    }
    else {
        $(".custom_header_row").last().after(row_str);
    }
}

// function to add a new custom proxy row to the proxies list
function add_custom_proxy_row(checked, disabled, id){
    // add the new proxy row http stucr to the proxy list
    let row_str = `<div class="input-group input-group-sm mb-3 custom_proxy_row" id="${id}">
                        <div class="input-group-prepend">
                            <div class="input-group-text custom_proxy_on_of custom_on_of">
                                <span data-toggle="tooltip" data-placement="top" title="Enable this option to use this a proxy server for anonymous browsing. This will turn off any other active custom proxy servers. Please note that using a proxy server may slow down your browsing speed.">
                                    <input type="checkbox" ${checked ? "checked" : ""}>
                                </span>
                            </div>
                        </div>

                        <input placeholder="host IP" type="text" class="form-control proxy_ip first custom_input">
                        <input placeholder="port" type="text" class="form-control proxy_port second custom_input">

                        <div class="input-group-append">
                            <button class="btn btn-primary custom_save ${disabled ? "disabled_cursor" : ""}" type="button" ${disabled ? "disabled" : ""}>Save</button>
                            <button class="btn btn-danger custom_delete" type="button">Delete</button>
                        </div>
                    </div>`

    if ($('.custom_proxy_row').length == 0){
        $(".custom_proxies").prepend(row_str);
    }
    else {
        $(".custom_proxy_row").last().after(row_str);
    }
}

// check if the input is a valid ip address
function is_valid_ip(ip) {
    let ip_regex = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    // match the ip address to the regex
    return ip_regex.test(ip);
}

// check if the input is a valid port number
function is_valid_port(port) {
    let port_regex = /^([0-9]{1,5})$/;
    // match the port number to the regex
    return port_regex.test(port);
}

// function to add the listeners to the custom items
function add_custom_row_listeners() {
    $(`.custom_input`).on("input", function () {
        // remove the invalid class on input
        $(this).removeClass("is-invalid");
        let parent_id = $(this).parent().attr("id");
        $(`#${parent_id} .invalid-feedback`).remove();

        // match the input to its original value and remove the unsaved_row class if they match
        if ($(this).attr("original_value") === $(this).val()) {
            $(this).parent().prop("classList").remove("unsaved_row")
        }
        else {
            $(this).parent().prop("classList").add("unsaved_row");
        }

        let this_first = $(this).parent().find(".first").val()
        let this_second = $(this).parent().find(".second").val()

        // disable the save button if either of custom_row inputs are empty
        if (this_first == "" || this_second == "") {
            $(this).parent().find(".custom_save").prop("disabled", true);
            $(this).parent().find(".custom_save").addClass("disabled_cursor");
        }
        // else enable the save button
        else {
            $(this).parent().find(".custom_save").prop("disabled", false);
            $(this).parent().find(".custom_save").removeClass("disabled_cursor");
        }
    });

    $('.custom_header_on_of').on("click", function () {
        let parent_id = $(this).parent().parent().attr("id");
        let checked = $(`#${parent_id} .custom_header_on_of input`).is(":checked").toString();

        // match the input to its original value and remove the unsaved_row class if they match
        if ($(this).attr("original_value") === checked) {
            $(this).parent().parent().prop("classList").remove("unsaved_row");
        }
        else {
            $(this).parent().parent().prop("classList").add("unsaved_row");
        }
    });

    $('.custom_proxy_on_of').on("click", async function () {
        // get the checked proxy from the db, the clicked proxy and the checked status of the clicked proxy
        let checked_proxy = await db.syncGet(cs.CHECKED_PROXY);
        let clicked_proxy = $(this).parent().parent().attr("id");
        let checked = $(`#${clicked_proxy} .custom_proxy_on_of input`).is(":checked").toString();

        // manage checked proxy if it is not the same as the clicked proxy
        if (checked_proxy && checked_proxy !== clicked_proxy) {
            // set the checked proxy to false
            await db.syncSet(`cp-${checked_proxy}`, {
                "key": $(`#${checked_proxy} .first`).val(),
                "value": $(`#${checked_proxy} .second`).val(),
                "id": checked_proxy,
                "checked": false
            });
            // set custom_proxy_on_of:checked to false and original_value to false
            $(`#${checked_proxy} .custom_proxy_on_of input`).prop("checked", false);
            $(`#${checked_proxy} .custom_proxy_on_of`).attr("original_value", false);
        }
        // if the clicked proxy is the same as the checked proxy, set checked to false
        if (checked_proxy === clicked_proxy) {
            checked = false;
        }
        // manage the clicked proxy
        await db.syncSet(`cp-${clicked_proxy}`, {
            "key": $(`#${clicked_proxy} .first`).val(),
            "value": $(`#${clicked_proxy} .second`).val(),
            "id": clicked_proxy,
            "checked": checked
        });
        // set custom_proxy_on_of:checked to checked and original_value to checked
        $(`#${clicked_proxy} .custom_proxy_on_of input`).prop("checked", checked);
        $(`#${clicked_proxy} .custom_proxy_on_of`).attr("original_value", checked);

        // set custom proxy in db based on the clicked proxy checkbox
        await db.syncSet(cs.CUSTOM_PROXY_STORAGE_KEY, {
            "host": checked ? $(`#${clicked_proxy} .first`).val() : "null",
            "port": checked ? $(`#${clicked_proxy} .second`).val() : "null",
            "checked": checked
        });

        // store new checked proxy id
        await db.syncSet(cs.CHECKED_PROXY, checked ? clicked_proxy : null);
    });

    $('.custom_save').on('click', async function (event) {
        // get the id of the parent div, the key and value of the custom row and the checked status of the custom row
        let id = $(this).parent().parent().attr("id");
        let is_proxy = id.startsWith("p");

        let key = $(`#${id} .first`).val();
        if (key.includes("_") || key.includes(" ")){
            key = key.replace(/_/g, "-").replace(/ /g, "-");
            $(`#${id} .first`).val(key);
        }
        let value = $(`#${id} .second`).val();
        if (value.includes(" ")){
            value = value.replace(/ /g, "-");
            $(`#${id} .second`).val(value);
        }

        let checked = $(`#${id} .custom_on_of input`).is(":checked");
    
        // check if the key and value are not empty
        if (!key || !value) {
            if (key == "") {
                $(`#${id} .first`).addClass("is-invalid");
            }
            if (value == "") {
                $(`#${id} .second`).addClass("is-invalid");
            }
        }
        // check if the key is a forbidden header [Accept-Language, User-Agent]
        else if (cs.FORBIDDEN_HEADERS.includes(key.toLowerCase())) {
            $(`#${id} .first`).addClass("is-invalid");
            $(`#${id} .first`).val("");
            $(`#${id}`).removeClass("unsaved_row")
            if ($(`#${id} .invalid-feedback`).length === 0) {
                $(`#${id}`).append(
                    `<div class="invalid-feedback">
                        Please, set your ${key.toLowerCase()} preference in the coresponding dropdown section!
                    </div>`
                )
            }
        }
        else if (is_proxy && !is_valid_ip(key)){
            $(`#${id} .first`).addClass("is-invalid");
            $(`#${id} .first`).val("");
            $(`#${id}`).removeClass("unsaved_row")
            if ($(`#${id} .invalid-feedback`).length === 0) {
                $(`#${id}`).append(
                    `<div class="invalid-feedback">
                        Please, use a valid IP address {0-255}.{0-255}.{0-255}.{0-255}!
                    </div>`
                )
            }
        }
        else if (is_proxy && !is_valid_port(value)){
            $(`#${id} .second`).addClass("is-invalid");
            $(`#${id} .second`).val("");
            $(`#${id}`).removeClass("unsaved_row")
            if ($(`#${id} .invalid-feedback`).length === 0) {
                $(`#${id}`).append(
                    `<div class="invalid-feedback">
                        Please, use a valid port number (0-65535)!
                    </div>`
                )
            }
        }
        else {
            // check if the custom row is a duplicate (different criteria for headers and proxies)
            let duplicate = await is_duplicate(id, key, value);

            // if duplicate, alert the user and remove the value from the input
            if (duplicate === true && id.includes("h")) {
                $(`#${id} .first`).addClass("is-invalid");
                $(`#${id} .first`).val("");
                $(`#${id}`).removeClass("unsaved_row")
                if ($(`#${id} .invalid-feedback`).length === 0) {
                    $(`#${id}`).append(
                        `<div class="invalid-feedback">
                            This header is already in use!
                        </div>`
                    )
                }
            }
            else if (duplicate === true && is_proxy) {
                $(`#${id} .second`).addClass("is-invalid");
                $(`#${id} .second`).val("");
                $(`#${id}`).removeClass("unsaved_row")
                if ($(`#${id} .invalid-feedback`).length === 0) {
                    $(`#${id}`).append(
                        `<div class="invalid-feedback">
                            This proxy ip address with given port is already in use!
                        </div>`
                    )
                }
            }
            // if not duplicate, save the custom row in the db and manage the attributes for change and UI behaviour
            else {
                let proxy_summary = {"key": key.toLowerCase(), "value": value, "id": id, "checked": checked}
                if (is_proxy){
                    await db.syncSet(cs.CUSTOM_PROXY_STORAGE_KEY, proxy_summary)
                }
                await db.syncSet(`c${id.substr(0,1)}-${id}`, proxy_summary);

                manage_attributes_for_change(id, key, value, checked);
                $(this).parent().parent().prop("classList").remove("unsaved_row");
            }
        }
    });

    $('.custom_delete').on('click', async function () {
        // get the id of the parent div and remove the custom row from the db
        let id = $(this).parent().parent().attr("id");
        let is_proxy = id.startsWith("p");
        let is_currently_used = $(`#${id} .custom_on_of input`).is(":checked");

        if (is_proxy && is_currently_used) {
            await db.syncSet(cs.CHECKED_PROXY, null);
            await db.syncSet(cs.CUSTOM_PROXY_STORAGE_KEY, {host: 'null', port: 'null'})
        }

        $(this).parent().parent().remove();
        
        // if there are no more custom rows, show the no_headers/no_proxies message
        if (!is_proxy) {
            if ($(".custom_header_row").length === 0) {
                $(".no_headers").css("display", "block");
            }
        }
        else if (is_proxy) {
            if ($(".custom_proxy_row").length === 0) {
                $(".no_proxies").css("display", "block");
            }
        }
        
        await db.syncRemove(`c${id.substr(0,1)}-${id}`);
    });
}

// function to assign smallest available id to a new custom row
function get_smallest_available_id(type) {
    let smallest_available = 1;

    // sort the custom rows by id
    let rows = $(`.custom_${type}_row`).sort(function (a, b) {
        return parseInt($(a).attr("id").substr(1)) - parseInt($(b).attr("id").substr(1));
    });

    // find the smallest available id
    for (let i = 0; i < rows.length; i++) {
        let id = parseInt(rows[i].id.substr(1));
        if (id === smallest_available + 1) {
            break;
        }
        smallest_available++;
    }
    return `${type.substr(0, 1)}${smallest_available}`;
}

// function to check if the custom row is a duplicate (different criteria for headers and proxies)
async function is_duplicate(id, new_key, new_value) {
    let storage_vals = await db.syncGetAll(`c${id.substr(0, 1)}-`);

    // check if the key is already in use
    if (id.includes('h')){ 
        for (let [key, val] of Object.entries(storage_vals)) {            
            if (val.key === new_key && val.id !== id) {
                return true
            }
        }
    }
    // check if the pair (proxy_ip, proxy_port) is already in use
    else if (id.includes('p')){ 
        for (let [key, val] of Object.entries(storage_vals)) {            
            if (val.key === new_key && val.value === new_value && val.id !== id) {
                return true
            }
        }
    }

    return false
}

// fucntion to update original_value attributes if custom row is changed
function manage_attributes_for_change(id, key, value, checked) {
    $(`#${id} .first`).attr("original_value", key);
    $(`#${id} .second`).attr("original_value", value);
    $(`#${id} .custom_on_of`).attr("original_value", checked);
}

// function hides "other" section if proxy or language/headers section is expanded
async function hide_setting_section(section) {
    $(`.btn-${section}_collapser`).attr("aria-expanded", false)
    $(`.btn-${section}_collapser`).addClass("collapsed")
    $(`#${section}_collapse`).removeClass("show")
    await db.localUpdateStates(`${section}`, false)
}

// function opens/closes UI sections based on the saved states before the extension was closed
async function mirror_ui_states() {
    let states = await db.localGet(cs.STATES_KEY);
    // iterate through the states and set the UI accordingly
    for (let state in states) {
        $(`.btn-${state}_collapser`).attr("aria-expanded", states[state])
        $(`.btn-${state}_collapser`).toggleClass("collapsed", !states[state])
        $(`#${state}_collapse`).toggleClass("show", states[state])
    }

    // if custom proxy is selected, disable the pre-defined proxy list
    if (states){
        $('#proxy_list').attr("disabled", states.custom_proxy)
    }

    // underline the currently opened section
    if (states?.headers){
        $('.btn-headers_collapser').addClass("settings_button_current")
    }
    else if (states?.proxy){
        $('.btn-proxy_collapser').addClass("settings_button_current")
    }
    else if (states?.profiles){
        $('.btn-profiles_collapser').addClass("settings_button_current")
    }
};

// create a snapshot of the current storage and save it to the storage as a profile
async function create_sync_storage_snapshot(profile_name) {
    // fetch important data from the storage
    let lang = await db.syncGet(cs.ACCEPT_LANGUAGE_STORAGE_KEY)
    let user_agent = await db.syncGet(cs.USER_AGENT_STORAGE_KEY)
    let custom_headers = await db.syncGetAll("ch-")

    let proxy = await db.syncGet(cs.PROXY_STORAGE_KEY)
    let custom_proxies = await db.syncGetAll("cp-")
    let custom_proxy = await db.syncGet(cs.CUSTOM_PROXY_STORAGE_KEY)
    let checked_proxy = await db.syncGet(cs.CHECKED_PROXY)
    let using_proxy = await db.syncGet(cs.USING_PROXY_STORAGE_KEY)

    // copy important values from the storage to the snapshot
    let snapshot = {}
    snapshot[cs.ACCEPT_LANGUAGE_STORAGE_KEY] = lang === undefined ? "null" : lang
    snapshot[cs.USER_AGENT_STORAGE_KEY] = user_agent === undefined ? "null" : user_agent

    snapshot[cs.PROXY_STORAGE_KEY] = proxy === undefined ? {host: 'null', port: 'null'} : proxy
    snapshot[cs.CUSTOM_PROXY_STORAGE_KEY] = custom_proxy === undefined ? {host: 'null', port: 'null'} : custom_proxy
    snapshot[cs.CHECKED_PROXY] = checked_proxy === undefined ? null : checked_proxy
    snapshot[cs.USING_PROXY_STORAGE_KEY] = using_proxy === undefined ? 'proxy' : using_proxy

    if (Object.entries(custom_proxies).length){
        // set the profile values to the storage
        for (const [index, key] of Object.entries(custom_proxies)) {
            snapshot[index] = key
        }
    }
    if (Object.entries(custom_headers).length){
        // set the profile values to the storage
        for (const [index, key] of Object.entries(custom_headers)) {
            snapshot[index] = key
        }
    }

    // if attempt to store the default profile witg PROFILE_PREXIF, remove the prefix
    let key = (profile_name === cs.PROFILES_STORAGE_KEY_PREFIX + cs.DEFAULT_PROFILE) ? cs.DEFAULT_PROFILE : profile_name
    // save the snapshot to the storage
    await db.syncSet(key, snapshot)
    return snapshot
}

// function to add a listener to the sometimes-displayed delete profile button
async function add_delete_profile_listener(name){

    $("#delete_profile").click(async function () {
        let current_profile_name = await db.syncGet(cs.CURRENT_PROFILE_STORAGE_KEY)
        
        // remove the deleted profile from the storage
        await db.syncRemove(name)
        
        if (name === current_profile_name){
            // set the default profile as the current profile
            await db.syncSet(cs.CURRENT_PROFILE_STORAGE_KEY, cs.DEFAULT_PROFILE)
            
            // get default profile from settings
            let default_profile = await db.syncGet(cs.DEFAULT_PROFILE)
            
            // apply the default profile to the storage
            await apply_profile(default_profile, cs.DEFAULT_PROFILE)
            
            
            // display the selected profile in the profile section
            await display_profile(default_profile, cs.DEFAULT_PROFILE)

            // update identity section
            await populate_langs_list()
            await populate_user_agent_list()
            await populate_custom_records(cs.HEADER)
            // if there are no custom headers, show the "no headers" message
            if (!$(".custom_header_row").length) {
                $(".no_headers").css("display", "block");
            }
            else {
                $(".no_headers").css("display", "none");
            }

            // update proxy section
            await populate_proxy_list()
            // populate the custom proxies
            await populate_custom_records(cs.PROXY)
            // if there are no custom proxies, show the "no proxies" message
            if (!$(".custom_proxy_row").length) {
                $(".no_proxies").css("display", "block");
            }
            else {
                $(".no_proxies").css("display", "none");
            }
        }
        else{
            // display the selected profile in the profile section
            await display_profile(await db.syncGet(current_profile_name), current_profile_name)
        }

        // update the profiles list
        await populate_profiles_list()
    });
}

// function to add a listener to the export profile button
async function add_export_profile_listener(name){
    $("#export_profile").click(async function () {
        let profile = await db.syncGet(name)
        let profile_json = JSON.stringify(profile, null, 4)
        let blob = new Blob([profile_json], {type: "octet/stream"});
        var url = URL.createObjectURL(blob);
        chrome.downloads.download(
            {
                url: url,
                filename: `${name.replace(cs.PROFILES_STORAGE_KEY_PREFIX, "")}.json`
            }
        );
    });
}

// functin to add a listener to the apply profile button
async function add_apply_profile_listener(name){
    $("#apply_profile").click(async function () {
        let current_profile_is_default = await db.syncGet(cs.CURRENT_PROFILE_STORAGE_KEY) === cs.DEFAULT_PROFILE
        let newly_selected_profile_is_not_default = name !== cs.DEFAULT_PROFILE

        // only take snapshot of profile-less settings if transitioning from default profile to a custom profile
        if (newly_selected_profile_is_not_default && current_profile_is_default){
                await create_sync_storage_snapshot(cs.DEFAULT_PROFILE)
            }
        // store which profile key corresponds to the currently selected profile
        await db.syncSet(cs.CURRENT_PROFILE_STORAGE_KEY, name)
        
          // get the selected profile from the storage based on the key
        let newly_selected_profile = await db.syncGet(name)
        // apply the selected profile to the storage
        await apply_profile(newly_selected_profile, name)
        
        // display the selected profile in the profile section
        await display_profile(newly_selected_profile, name)
        display_profile_name(name)

        $('.btn-proxy_collapser').attr("disabled", newly_selected_profile_is_not_default)
        $('.btn-proxy_collapser').toggleClass("disabled_cursor", newly_selected_profile_is_not_default)
        $('.btn-headers_collapser').attr("disabled", newly_selected_profile_is_not_default)
        $('.btn-headers_collapser').toggleClass("disabled_cursor", newly_selected_profile_is_not_default)
        $('#add_new_profile').prop("hidden", newly_selected_profile_is_not_default)
        
        if (!newly_selected_profile_is_not_default){
            // update identity section
            await populate_langs_list()
            await populate_user_agent_list()
            await populate_custom_records(cs.HEADER)
            // if there are no custom headers, show the "no headers" message
            if (!$(".custom_header_row").length) {
                $(".no_headers").css("display", "block");
            }
            else {
                $(".no_headers").css("display", "none");
            }

            // update proxy section
            await populate_proxy_list()
            // populate the custom proxies
            await populate_custom_records(cs.PROXY)
            // if there are no custom proxies, show the "no proxies" message
            if (!$(".custom_proxy_row").length) {
                $(".no_proxies").css("display", "block");
            }
            else {
                $(".no_proxies").css("display", "none");
            }

        }

    });
}

// function to dispaly currently chosen profile in the profile section
async function display_profile(profile, profile_name){
    // TODO: populate proxy, lang, custom headers, custom proxies and other UI sections (?)
    $('.profile_table').remove();
    $('#delete_profile').remove();
    $('#export_profile').remove();
    $('#apply_profile').remove();

    let curr_profile_name = await db.syncGet(cs.CURRENT_PROFILE_STORAGE_KEY)
    let displayed_profile_is_not_current_profile = profile_name !== curr_profile_name
    let current_profile = await db.syncGet(curr_profile_name)

    let table_begin = `<table class="table table-sm table-bordered table-striped table-hover profile_table">
                        <thead>
                            <tr>
                                <th class="key_col" scope="col">Setting</th>
                                <th class="val_col" scope="col">Value</th>
                                ${displayed_profile_is_not_current_profile ? '<th class="diff_col" scope="col">Currently</th>' : ''}
                            </tr>
                        </thead>
                        <tbody>`
    let table_content = ''
    let table_end = '</tbody></table>'
    let using_custom_proxy = profile[cs.USING_PROXY_STORAGE_KEY] === cs.CUSTOM_PROXY_STORAGE_KEY
    let using_predefined_proxy = profile[cs.USING_PROXY_STORAGE_KEY] === cs.PROXY_STORAGE_KEY
    $.each(profile, function(key, value){
        if (value !== null && value !== "null" && 
            ((typeof value !== "object") || (typeof value === "object" && value.key !== "null" && value.value !== "null" && value.host !== "null" && value.port !== "null"))){   
            if (key === cs.ACCEPT_LANGUAGE_STORAGE_KEY){
                table_content += `<tr>
                                    <td class="key_col">Language</td>
                                    <td class="val_col">${value}</td>
                                    ${displayed_profile_is_not_current_profile ? `<td ${current_profile[key] !== value ? "class='col_with_difference'" : ''}>${current_profile[key] === "null" ? "-" : current_profile[key]}</td>` : ''}
                                </tr>`
            }
            else if (key === cs.USER_AGENT_STORAGE_KEY){
                table_content += `<tr>
                                    <td class="key_col">Device</td>
                                    <td class="val_col ua_col">${value}</td>
                                    ${displayed_profile_is_not_current_profile ? `<td ${current_profile[key] !== value ? "class='col_with_difference ua_col'" : 'class="ua_col"'}>${current_profile[key] === "null" ? "-" : current_profile[key]}</td>` : ''}
                                </tr>`
            }
            else if (key === cs.PROXY_STORAGE_KEY){
                table_content += `<tr ${key === profile[cs.USING_PROXY_STORAGE_KEY] ? "class='active_proxy' " : ""}>
                                    <td class="key_col">Proxy</td>
                                    <td class="val_col">${value.host}<i style="font-size: smaller;">:${value.port}</i></td>
                                    ${displayed_profile_is_not_current_profile ? `<td class="diff_col ${(using_custom_proxy || current_profile[key].host !== value.host || current_profile[key].port !== value.port) ? 'col_with_difference' : ''}">${current_profile[key].host === "null" ? "-" : current_profile[key].host}<i style="font-size: smaller;">:${current_profile[key].port === "null" ? "-" : current_profile[key].port}</i></td>` : ''}
                                </tr>`
            }
            else if (key.startsWith("cp-")){
                table_content += `<tr ${using_custom_proxy && value.checked ? "class='active_proxy' " : ""} >
                                    <td class="key_col">Custom Proxy</td>
                                    <td class="val_col">${value.key}<i style="font-size: smaller;">:${value.value} (${value.checked ? "On" : "Off"})</i></td>
                                    ${displayed_profile_is_not_current_profile ? `<td class="diff_col ${!(key in current_profile) ||
                                                                                                        current_profile[key].key !== value.key ||
                                                                                                        current_profile[key].value !== value.value ||
                                                                                                        current_profile[key].checked !== value.checked ? 'col_with_difference' : ''}">
                                                                                                        ${!(key in current_profile) || current_profile[key].key === "null" ? "-" : current_profile[key].key}<i style="font-size: smaller;">:${!(key in current_profile) || current_profile[key].value === "null" ? "-" : current_profile[key].value} (${key in current_profile && current_profile[key]?.checked ? "On" : "Off"})</i></td>` : ''}
                                </tr>`
            }
            else if (key.startsWith("ch-")){
                table_content += `<tr>
                                    <td class="key_col">Header</td>
                                    <td class="val_col">
                                        <ul class="profile_ul">
                                            <li><b>Name:</b> ${value.key}</li>
                                            <li><b>Value:</b> ${value.value}</li>
                                            <li><b>Active:</b> ${value.checked ? "Yes" : "No"}</li>
                                        </ul>
                                    </td>
                                    ${displayed_profile_is_not_current_profile ? `
                                    <td class="diff_col ${key in current_profile && 
                                                        (current_profile[key].key !== value.key ||
                                                        current_profile[key].value !== value.value || 
                                                        current_profile[key].checked !== value.checked) ? 'col_with_difference' : ''}">
                                        <ul class="profile_ul">
                                            <li><b>Name:</b> ${key in current_profile ? current_profile[key].key : "-" }</li>
                                            <li><b>Value:</b> ${key in current_profile ? current_profile[key].value : "-"}</li>
                                            <li><b>Active:</b> ${key in current_profile ? (current_profile[key].checked ? "Yes" : "No") : "-"}</li>
                                        </ul>
                                    </td>` : ''}
                                  </tr>`
            }
        }
    })
    $(".profile_preview_table").append(`${table_begin}${table_content}${table_end}`);

    // add export profile button
    $(".profile_preview").append(
        `
        <button class="btn btn-primary custom_export" id="export_profile" type="button">Export profile</button>
        `
    )
    add_export_profile_listener(profile_name)
    
    // add apply profile button
    if (curr_profile_name !== profile_name){
        $(".profile_preview").append(
            `
            <button class="btn btn-primary custom_apply" id="apply_profile" type="button">Use profile</button>
            `
        )
        add_apply_profile_listener(profile_name)
    }

    // add delete button
    if (profile_name !== cs.DEFAULT_PROFILE){
        $(".profile_preview").append(
            `
            <button class="btn btn-danger delete_profile" id="delete_profile" type="button">Delete profile</button>
            `
            )
        add_delete_profile_listener(profile_name)
    }

}

function capitalize(str){
    return str.charAt(0).toUpperCase()+str.slice(1);
}

// display the profile name on top of popup
function display_profile_name(profile_name){
    if (profile_name === cs.DEFAULT_PROFILE){
        $(".current_profile").prop("hidden", true)
    }   
    else{
        $(".current_profile").prop("hidden", false)
        let profile_name_to_display = capitalize(profile_name.replace(cs.PROFILES_STORAGE_KEY_PREFIX, ""))
        $(".profile_name").text(profile_name_to_display)
    }
}


// function to apply the profile to the storage
async function apply_profile(profile, name){
    let cps = await db.syncGetAll("cp-")
    let chs = await db.syncGetAll("ch-")
    // Apply identity tab =====================================================================================================
    // apply predefined values
    await db.syncSet(cs.ACCEPT_LANGUAGE_STORAGE_KEY, profile[cs.ACCEPT_LANGUAGE_STORAGE_KEY])
    await db.syncSet(cs.USER_AGENT_STORAGE_KEY, profile[cs.USER_AGENT_STORAGE_KEY])

    // rewrite custom headers shared by current and new profile
    let custom_headers = Object.keys(profile).filter(v => v.startsWith('ch-'));
    for (const [index, key] of Object.entries(custom_headers)) {
        if (key in chs){
            delete chs[key]
        }
        await db.syncSet(key, profile[key]) 
    }

    // remove all custom headers that are not in the new profile
    for (const [index, key] of Object.entries(chs)) {
        await db.syncRemove(index)
    }

    // Apply proxy tab ========================================================================================================
    // apply proxy meta values
    await db.syncSet(cs.PROXY_STORAGE_KEY, profile[cs.PROXY_STORAGE_KEY])
    await db.syncSet(cs.CUSTOM_PROXY_STORAGE_KEY, profile[cs.CUSTOM_PROXY_STORAGE_KEY])
    await db.syncSet(cs.CHECKED_PROXY, profile[cs.CHECKED_PROXY])

    // store information about currently used proxy
    await db.syncSet(cs.USING_PROXY_STORAGE_KEY, profile[cs.USING_PROXY_STORAGE_KEY])

    // rewrite custom proxies shared by current and new profile
    let custom_proxies = Object.keys(profile).filter(v => v.startsWith('cp-'));
    for (const [index, key] of Object.entries(custom_proxies)) {
        if (key in cps){
            delete cps[key]
        }
        await db.syncSet(key, profile[key])
    }
    // remove all custom proxies that are not in the new profile
    for (const [index, key] of Object.entries(cps)) {
        await db.syncRemove(index)
    }
}

// event listeners ===========================================================================================================
// save current storage settings as a profile
$(".new_profile_submit").click(async function () {
    // obtain all existing profiles
    let existing_profiles = await db.syncGetAll(cs.PROFILES_STORAGE_KEY_PREFIX)
    
    // check if profile file was uploaded
    const file = document.getElementById('file_upload_input').files[0];
    
    // get the name of the new profile
    let profile_name = $(".profile_name_input").val().replace(" ", "_");
    $(`.profile_name_input`).val("")
    $(".new_profile_submit").prop("disabled", true);
    $(".new_profile_submit").toggleClass("disabled_cursor", true)

    // if profile name is a duplicate, display error message
    if (cs.PROFILES_STORAGE_KEY_PREFIX + profile_name in existing_profiles) {
        $("#add_new_profile").append(
            `<div class="invalid-feedback">
                Profile with this name already exists!
            </div>`
        )
        $(this).prop("disabled", true);
        $(this).addClass("disabled_cursor")
        $(`.profile_name_input`).addClass("is-invalid");        
    }
    else{
        // create the profile from file
        if (file){
            // read the file
            let reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            document.getElementById('file_upload_input').value = '';

            reader.onload = async function (evt) {
                // parse the file
                let profile = JSON.parse(evt.target.result);

                // if profile is valid, save it
                if (pv.validate_profile_structure(profile)){
                    $("#add_new_profile")
                    .append(`<div class="valid-feedback">Profile created from ${file ? "file": "current settings"}! </div>`)
                    .addClass("was-validated");
                    
                    // save the profile
                    await db.syncSet(cs.PROFILES_STORAGE_KEY_PREFIX + profile_name, profile)
                    await populate_profiles_list()
                }
                // if profile is not valid, display error message
                else{
                    $("#add_new_profile")
                    .append(`<div class="invalid-feedback">Profile file is not valid! </div>`)
                    .addClass("was-validated");

                    $(this).prop("disabled", true);
                    $(this).addClass("disabled_cursor")
                    $(`.profile_name_input`).addClass("is-invalid");

                }

                // remove the result messages after some time
                setTimeout(function () {
                    $("#add_new_profile").removeClass("was-validated");
                    $("#add_new_profile .valid-feedback").remove()
                    $("#add_new_profile .invalid-feedback").remove()
                }, cs.VALIDATION_SUCCESS_DURATION);
            }

        }
        // create the profile from current settings
        else{
            await create_sync_storage_snapshot(cs.PROFILES_STORAGE_KEY_PREFIX + profile_name)
            $("#add_new_profile")
            .append(`<div class="valid-feedback">Profile created from ${file ? "file": "current settings"}! </div>`)
            .addClass("was-validated");

            await populate_profiles_list()
            
            setTimeout(function () {
                $("#add_new_profile").removeClass("was-validated");
                $("#add_new_profile .valid-feedback").remove()
                $("#add_new_profile .invalid-feedback").remove()
            }, cs.VALIDATION_SUCCESS_DURATION);
        }
    }
    
});

// detect changes in the new_profile name input
$(`.profile_name_input`).on("input", async function () {
    // enable button on non-empty value
    let profile_name = $(this).val();
    $(this).removeClass("is-invalid");
    $(".new_profile_submit").prop("disabled", profile_name.length === 0);
    $(".new_profile_submit").toggleClass("disabled_cursor", profile_name.length)
    $("#add_new_profile .invalid-feedback").remove()
});

// detect changes in the profiels list
$('#profiles_list').change(async function () {
    // get the selected profile
    var newly_selected_profile_name = $('option:selected', this).attr('value') !== "null" ? $('option:selected', this).attr('value') : cs.DEFAULT_PROFILE;

    // get the selected profile from the storage based on the key
    let newly_selected_profile = await db.syncGet(newly_selected_profile_name)
    // display the selected profile in the profile section
    await display_profile(newly_selected_profile, newly_selected_profile_name)
});


// detect changes in the proxy list
$('#proxy_list').change(async function () {
    // get the selected proxy and port
    var selected_proxy = $('option:selected', this).attr('value');
    var selected_port = $('option:selected', this).attr('port');


    let store = {
        host: selected_proxy,
        port: selected_port,
    }
    // save the selected proxy in the db to notify the background script
    await db.syncSet(cs.PROXY_STORAGE_KEY, store);
    // save the fact that the user is using a pre-defined proxy in the db to notify the background script
    await db.syncSet(cs.USING_PROXY_STORAGE_KEY, cs.PROXY_STORAGE_KEY)
});

// detect changes in the language list
$('#langs_list').change(async function () {
    // get the selected language and save it in the db to notify the background script
    var selected_lang = $('option:selected', this).attr('value');
    await db.syncSet(cs.ACCEPT_LANGUAGE_STORAGE_KEY, selected_lang);
});

// detect changes in the user agent list
$('#user_agents_list').change(async function () {
    // get the selected user agent and save it in the db to notify the background script
    var selected_user_agent = $('option:selected', this).attr('value');
    await db.syncSet(cs.USER_AGENT_STORAGE_KEY, selected_user_agent);
});    

// detect clicks on the "add header" button
$(".add-btn-headers").on("click", function () {
    // get the smallest available id for the new custom header row
    let id = get_smallest_available_id("header");
    // add the new custom header row and add event listeners to it
    add_custom_header_row(true, true, id)
    $(`#${id}`).addClass("unsaved_row")
    add_custom_row_listeners();

    // if this is the first custom header row, hide the "no headers" message
    if ($(".custom_header_row").length === 1) {
        $(".no_headers").css("display", "none")
    }
});

// detect clicks on the "add proxy" button
$(".add-btn-proxy").on("click", function () {
    // get the smallest available id for the new custom proxy row
    let id = get_smallest_available_id(cs.PROXY);
    // add the new custom proxy row and add event listeners to it
    add_custom_proxy_row(false, true, id)
    $(`#${id}`).addClass("unsaved_row")
    add_custom_row_listeners();
    // if this is the first custom proxy row, hide the "no proxies" message
    if ($(".custom_proxy_row").length === 1) {
        $(".no_proxies").css("display", "none")
    }
});

// detect clicks on the buttons that opens/closes the headers section
$('.btn-headers_collapser').on("click", async function () {
    // toggle the state of the headers section in the db
    await db.localToggleState(cs.HEADER)
    // hide the proxy section if it is open
    await hide_setting_section(cs.PROXY)
    // hide the profiles section if it is open
    await hide_setting_section(cs.PROFILE)

    // underline the currently opened section
    $('.btn-headers_collapser').toggleClass("settings_button_current")
    $('.btn-proxy_collapser').removeClass("settings_button_current")
    $('.btn-profiles_collapser').removeClass("settings_button_current")
});

// detect clicks on the buttons that opens/closes the proxy section
$('.btn-proxy_collapser').on("click", async function () {
    // toggle the state of the proxy section in the db
    await db.localToggleState(cs.PROXY)
    // hide the headers section if it is open
    await hide_setting_section(cs.HEADER)
    // hide the profiles section if it is open
    await hide_setting_section(cs.PROFILE)
    
    // underline the currently opened section
    $('.btn-proxy_collapser').toggleClass("settings_button_current")
    $('.btn-headers_collapser').removeClass("settings_button_current")
    $('.btn-profiles_collapser').removeClass("settings_button_current")
});

// detect clics on the buttons that opens/closes the pofiles section
$('.btn-profiles_collapser').on("click", async function () {
    // toggle the state of the profiles section in the db
    await db.localToggleState(cs.PROFILE)
    // hide the headers section if it is open
    await hide_setting_section(cs.HEADER)
    // hide the proxy section if it is open
    await hide_setting_section(cs.PROXY)

    await create_sync_storage_snapshot(cs.DEFAULT_PROFILE)
    await populate_profiles_list()

    // underline the currently opened section
    $('.btn-profiles_collapser').toggleClass("settings_button_current")
    $('.btn-proxy_collapser').removeClass("settings_button_current")
    $('.btn-headers_collapser').removeClass("settings_button_current")
});


// detect clicks on the buttons that opens/closes the custom headers section
$('.btn-custom_headers_collapser').on("click", async function () {
    // toggle the state of the custom headers section in the db
    await db.localToggleState("custom_headers")
});

// detect clicks on the buttons that opens/closes the custom proxies section
$('.btn-custom_proxy_collapser').on("click", async function () {
    // toggle the state of the custom proxies section in the db
    let custom_proxies_on = await db.localToggleState("custom_proxy")
    // if custom proxies are enabled, disable the pre-defined proxy list
    $('#proxy_list').attr("disabled", custom_proxies_on)

    // save the fact if the user is using a custom proxy or a predefined one in the db to notify the background script
    if (custom_proxies_on) {
        await db.syncSet(cs.USING_PROXY_STORAGE_KEY, cs.CUSTOM_PROXY_STORAGE_KEY)
    }
    else {
        await db.syncSet(cs.USING_PROXY_STORAGE_KEY, cs.PROXY_STORAGE_KEY)
    }

});

// main ======================================================================
window.onload = async function () {
    // populate the pre-defined language list
    await populate_langs_list()
    // populate the pre-defined proxy list
    await populate_proxy_list()
    // populate the pre-defined user agent list
    await populate_user_agent_list()

    // populate the custom headers
    await populate_custom_records(cs.HEADER)

    // if there are no custom headers, show the "no headers" message
    if (!$(".custom_header_row").length) {
        $(".no_headers").css("display", "block");
    }
    else {
        $(".no_headers").css("display", "none");
    }

    // populate the custom proxies
    await populate_custom_records(cs.PROXY)
    // if there are no custom proxies, show the "no proxies" message
    if (!$(".custom_proxy_row").length) {
        $(".no_proxies").css("display", "block");
    }
    else {
        $(".no_proxies").css("display", "none");
    }

    // fill the list of profiles and display current profile
    await populate_profiles_list()

    // mirror the state of the toggles before the extension was closed
    await mirror_ui_states()
};