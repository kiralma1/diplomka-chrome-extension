// popup constansts ================================================================
const PROXY_STORAGE_KEY = "proxy"
const CUSTOM_PROXY_STORAGE_KEY = "custom_proxy"
const USING_PROXY_STORAGE_KEY = "using_proxy"
const USER_AGENT_STORAGE_KEY = "user_agent"
const ACCEPT_LANGUAGE_STORAGE_KEY = "ac-lang";

const CHECKED_PROXY = "checked_proxy"
const STATES_KEY = "ui_states"
const HEADER = "headers"
const PROXY = "proxy"
const PROFILE = "profiles"

const ACCEPT_LANGUAGE_HEADER = "Accept-Language";
const EXTRA_HEADERS_HEADER = "Extra-Headers";
const USER_AGENT_HEADER = "User-Agent";
const FORBIDDEN_HEADERS = [ACCEPT_LANGUAGE_HEADER.toLowerCase(), USER_AGENT_HEADER.toLowerCase()]

const VALIDATION_SUCCESS_DURATION = 2000;
// background constants =============================================================
const REMOVE_ACTION = "remove";
const SET_ACTION = "set";
const MODIFY_HEADERS_ACTION = "modifyHeaders";

// database constants ==============================================================
const PROXIES_DB = "proxies_db";
const LANGS_DB = "langs_db";
const USER_AGENTS_DB = "user_agents_db";
const GSP_EXCLUSIVE_PROXIES_DB = "gsp_exclusive_proxies_db";

// alarm names =====================================================================
const DATA_DOWNLOAD_ALARM = "data_download_alarm";
const DATA_DOWNLOAD_PERIOD_STORAGE_KEY = "data_download_period";
const DATA_DEFAULT_DOWNLOAD_PERIOD = 24 * 60; // minutes --> 1 day

const PROXY_PING_ALARM = "proxy_ping_alarm";
const PROXY_PING_PERIOD_STORAGE_KEY = "proxy_ping_period";
const PROXY_DEFAULT_PING_PERIOD = 15; // minutes

// gitlab files ====================================================================
const PROXIES_URL = "https://gitlab.com/marek.kiraly-sf/gsp-chrome-extension-sources/-/raw/main/proxies.json"
const LANGS_URL = "https://gitlab.com/marek.kiraly-sf/gsp-chrome-extension-sources/-/raw/main/langs.json"
const USER_AGENTS_URL = "https://gitlab.com/marek.kiraly-sf/gsp-chrome-extension-sources/-/raw/main/user_agents.json"
const GSP_EXCLUSIVE_PROXIES_URL = "https://gitlab.fit.cvut.cz/kiralma1/diplomka-chrome-extension/-/raw/master/src/gsp_exclusive_proxies.json"


// PROFILES ========================================================================
const CURRENT_PROFILE_STORAGE_KEY = "current_profile";
const DEFAULT_PROFILE = "default";
const PROFILES_STORAGE_KEY_PREFIX = "prof-";
const DISPLAY_LIST = [
    ACCEPT_LANGUAGE_STORAGE_KEY,
    USER_AGENT_STORAGE_KEY,
]
// make the constants available to the rest of the extension
export default{
    PROXY_STORAGE_KEY,
    CUSTOM_PROXY_STORAGE_KEY,
    USING_PROXY_STORAGE_KEY,
    USER_AGENT_STORAGE_KEY,
    ACCEPT_LANGUAGE_STORAGE_KEY,
    CHECKED_PROXY,
    STATES_KEY,
    HEADER,
    PROXY,
    PROFILE,
    ACCEPT_LANGUAGE_HEADER,
    EXTRA_HEADERS_HEADER,
    USER_AGENT_HEADER,
    FORBIDDEN_HEADERS,
    REMOVE_ACTION,
    SET_ACTION,
    MODIFY_HEADERS_ACTION,
    VALIDATION_SUCCESS_DURATION,
    
    PROXIES_DB,
    LANGS_DB,
    USER_AGENTS_DB,
    GSP_EXCLUSIVE_PROXIES_DB,

    DATA_DOWNLOAD_ALARM,
    DATA_DOWNLOAD_PERIOD_STORAGE_KEY,
    DATA_DEFAULT_DOWNLOAD_PERIOD,

    PROXY_PING_ALARM,
    PROXY_PING_PERIOD_STORAGE_KEY,
    PROXY_DEFAULT_PING_PERIOD,

    PROXIES_URL,
    LANGS_URL,
    USER_AGENTS_URL,
    GSP_EXCLUSIVE_PROXIES_URL,

    CURRENT_PROFILE_STORAGE_KEY,
    DEFAULT_PROFILE,
    PROFILES_STORAGE_KEY_PREFIX,
    DISPLAY_LIST,
}