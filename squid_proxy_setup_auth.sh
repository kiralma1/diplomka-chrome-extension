#!/bin/bash

# AUTHOR: Marek Kiraly
# SCRIPT TO SETUP SQUID OPEN PROXY ON UBUNTU 20.04 WITH STEP BY STEP COMMENTS

GREEN='\033[0;32m'
NC='\033[0m' # No Color

# UPDATE AND INSTALL SQUID
sudo apt-get update;
sudo apt-get install --yes squid;
sudo apt-get install --yes apache2-utils;
echo -e "${GREEN}Squid installed ---------------------------------------------${NC}";

# PREPARING SQUID CONFIG AND PROTECTING ORIGINAL
echo -e "${GREEN}Preparing squid config --------------------------------------${NC}";
sudo cp /etc/squid/squid.conf /etc/squid/squid.conf.original;
sudo chmod a-w /etc/squid/squid.conf.original;
ls -l /etc/squid/squid.conf*;
echo -e "${GREEN}Squid config prepared ---------------------------------------${NC}";

# SQUID CONFIG CHANGES
echo -e "${GREEN}Applying squid config changes -------------------------------${NC}";

sed -i 's|##auth_param basic program <uncomment and complete this line>|auth_param basic program /usr/lib/squid/basic_ncsa_auth /etc/squid/squid.passwd|' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "auth_param basic program /usr/lib/squid/basic_ncsa_auth /etc/squid/squid.passwd";

sed -i 's/##auth_param basic children 5 startup=5 idle=1/auth_param basic children 5/' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "auth_param basic children 5";

sed -i 's/##auth_param basic realm Squid proxy-caching web server/auth_param basic realm Squid proxy-caching web server/' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "auth_param basic realm Squid proxy-caching web server";

sed -i 's/##auth_param basic credentialsttl 2 hours/auth_param basic credentialsttl 2 hours/' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "auth_param basic credentialsttl 2 hours";

sed -i '/auth_param basic credentialsttl 2 hours/ a\
acl auth_users proxy_auth REQUIRED\
http_access allow auth_users' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "acl auth_users proxy_auth REQUIRED";
cat /etc/squid/squid.conf | grep "http_access allow auth_users";

sed -i 's/# dns_v4_first off/dns_v4_first on/' /etc/squid/squid.conf;
cat /etc/squid/squid.conf | grep "dns_v4_first on";

echo -e "${GREEN}Squid config changes applied --------------------------------${NC}";

# ADD USER
password="Praha2023Zurych";
echo -e "$password\n$password" | sudo htpasswd -c /etc/squid/squid.passwd admin
echo -e "${GREEN}User added --------------------------------${NC}";

# REMOVE REDUNDANT LINES    
sed -Ei '/(^$|^#)/d' /etc/squid/squid.conf
echo -e "${GREEN}Redundant lines removed --------------------------------${NC}";

# RESTART SQUID TO APPLY CHANGES AND CHECK STATUS
echo -e "${GREEN}Restarting squid and checking status ------------------------${NC}";
systemctl restart squid;
systemctl status squid | grep "Active:";
echo -e "${GREEN}Squid restarted and status checked --------------------------${NC}";
